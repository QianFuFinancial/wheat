#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: tests/unit/test_utils.py
# Author: Jimin Huang <huangjimin@whu.edu.cn>
# Date: 14.11.2017
import os

from nose.tools import assert_equals

from wheat import utils


class TestDirectoriesToCreate(object):
    """Test class for ``utils.directories_to_create``"""

    def setUp(self):
        self.fake_project_name = "test"

        self.expect_directories = [
            "test",
            os.path.join("test", "test"),
            os.path.join("test", "CHANGES"),
            os.path.join("test", "tests"),
            os.path.join("test", "chart"),
            os.path.join("test", "chart", "charts"),
            os.path.join("test", "chart", "templates"),
            os.path.join("test", "chart", "templates", "tests"),
            os.path.join("test", "test", "conf"),
            os.path.join("test", "tests", "unit"),
            os.path.join("test", "tests", "integration"),
            os.path.join("test", "tests", "functional"),
            os.path.join("test", "test", "models"),
        ]

    def test_directories_to_create(self):
        """Check if ``utils.directories_to_create`` works"""
        assert_equals(
            utils.directories_to_create(self.fake_project_name),
            self.expect_directories,
        )


class TestFilesToGenerate(object):
    """Test class for ``utils.files_to_generate``"""

    def setUp(self):
        self.fake_project_name = "test"
        self.expect_files = [
            os.path.join("test", "Dockerfile"),
            os.path.join("test", "setup.cfg"),
            os.path.join("test", ".gitignore"),
            os.path.join("test", "README.md"),
            os.path.join("test", ".gitlab-ci.yml"),
            os.path.join("test", ".pre-commit-config.yaml"),
            os.path.join("test", "pyproject.toml"),
            os.path.join("test", "CHANGES.rst"),
            os.path.join("test", "test", "main.py"),
            os.path.join("test", "test", "constant.py"),
            os.path.join("test", "test", "utils.py"),
            os.path.join("test", "test", "__init__.py"),
            os.path.join("test", "test", "models", "__init__.py"),
            os.path.join("test", "CHANGES", "TEMPLATE.rst"),
            os.path.join("test", "chart", "Chart.yaml"),
            os.path.join("test", "chart", "values.yaml"),
            os.path.join("test", "chart", "templates", "secret.yaml"),
            os.path.join("test", "chart", "templates", "deployment.yaml"),
            os.path.join("test", "chart", "templates", "NOTES.txt"),
            os.path.join("test", "chart", "templates", "service.yaml"),
            os.path.join("test", "chart", "templates", "serviceaccount.yaml"),
            os.path.join("test", "chart", "templates", "_helpers.tpl"),
            os.path.join("test", "tests", "unit", "__init__.py"),
            os.path.join("test", "tests", "unit", "test_utils.py"),
            os.path.join("test", "tests", "integration", "__init__.py"),
            os.path.join(
                "test", "tests", "integration", "test_main_integration.py"
            ),
            os.path.join("test", "tests", "functional", "__init__.py"),
            os.path.join(
                "test", "tests", "functional", "test_main_functional.py"
            ),
            os.path.join("test", "test", "conf", "logging.yaml"),
            os.path.join("test", "test", "conf", "__init__.py"),
        ]

    def test_files_to_generate(self):
        """Check if ``utils.files_to_generate`` works"""
        assert_equals(
            utils.files_to_generate(self.fake_project_name, ""),
            self.expect_files,
        )

    def test_files_to_generate_with_data_mid(self):
        """Check if ``utils.files_to_generate`` works with data_mid"""
        self.expect_files.extend(
            [
                os.path.join(
                    "test", "tests", "functional", "test_data_service.py"
                ),
            ]
        )

        assert_equals(
            utils.files_to_generate(self.fake_project_name, "test"),
            self.expect_files,
        )


def test_generate_match_rules():
    """Check if ``utils.generate_match_rules`` works"""
    fake_files = [
        "test.py",
        os.path.join("test", "test.py"),
        os.path.join("test", "test", "models", "statistics.py"),
    ]

    expect_match_rules = {
        "test.py": "test.py.template",
        os.path.join("test", "test.py"): "test.py.template",
        os.path.join(
            "test", "test", "models", "statistics.py"
        ): "statistics.py.template",
    }

    assert_equals(utils.generate_match_rules(fake_files), expect_match_rules)


def test_parse_yes_no():
    """Check if `utils.parse_yes_no` works"""
    user_inputs = "yYnN"
    expect = [True, True, False, False]
    for user_input, expect_result in zip(user_inputs, expect):
        assert_equals(utils.parse_yes_no(user_input), expect_result)
