1.1.4 (2020-11-04)
==================

Bugfixes
--------

- - fix releaseName not in chart tpl
  - fix secret.yaml not in non-data projects
  - remove useless cache in ci config
  - fix deployment container args
  `#48 <https://git.chancefocus.com/transaction/common/wheat/issues/48>`_


----


1.1.3 (2020-11-02)
==================

Bugfixes
--------

- Fix cli project without data service.
  `#47 <https://git.chancefocus.com/transaction/common/wheat/issues/47>`_


----


1.1.2 (2020-10-28)
==================

Bugfixes
--------

- - fix deployment nacos issues
  - fix stage order
  - fix pages build issues
  - accelerate ci
  `#46 <https://git.chancefocus.com/transaction/common/wheat/issues/46>`_


----


1.1.1 (2020-03-31)
==================

Bugfixes
--------

- Fix chance-config issue
  Fix YamClient config issue
  `#45 <https://git.chancefocus.com/transaction/common/wheat/issues/45>`_


----


1.1.0 (2020-03-30)
==================

Features
--------

- Add pre-commit-config file
  Add nacos variables to helm
  `#39 <https://git.chancefocus.com/transaction/common/wheat/issues/39>`_
- Add nacos secret template
  `#42 <https://git.chancefocus.com/transaction/common/wheat/issues/42>`_
- Update app initialization
  `#44 <https://git.chancefocus.com/transaction/common/wheat/issues/44>`_


Bugfixes
--------

- Move sphinx and sphinx_rtd_theme from dev-requirements to requirements
  `#38 <https://git.chancefocus.com/transaction/common/wheat/issues/38>`_
- Fix generated files flake8 issue
  `#42 <https://git.chancefocus.com/transaction/common/wheat/issues/42>`_


Misc
----

- `#42 <https://git.chancefocus.com/transaction/common/wheat/issues/42>`_


----


1.0.0 (2020-03-26)
==================

Features
--------

- Support generating fastapi project
  `#32 <https://git.chancefocus.com/transaction/common/wheat/issues/32>`_
- Add redis and data options
  `#32 <https://git.chancefocus.com/transaction/common/wheat/issues/32>`_


Misc
----

- Disable cli projects' ingress service
  `#32 <https://git.chancefocus.com/transaction/common/wheat/issues/32>`_


----


0.3.1 (2020-03-25)
==================

- Add data connection service choice based on chance-yam
  Remove other requirements
  `#36 <https://git.chancefocus.com/transaction/common/wheat/issues/36>`_


----


0.3.0 (2020-03-24)
==================

Features
--------

- Introducing poetry to dependency management and publish
  Remove chance-orm related content in templates
  `#28 <https://git.chancefocus.com/transaction/common/wheat/issues/28>`_


Improved Documentation
----------------------

- Add poetry doc in README
  `#28 <https://git.chancefocus.com/transaction/common/wheat/issues/28>`_


----


0.1.5 (2020-03-24)
==================

Features
--------

- Add towncrier support
  `#30 <https://git.chancefocus.com/transaction/common/wheat/issues/30>`_


Deprecations and Removals
-------------------------

- Remove install_packages
  `#30 <https://git.chancefocus.com/transaction/common/wheat/issues/30>`_


----
