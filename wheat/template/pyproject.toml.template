[tool.poetry]
name = "{PROJECT_NAME}"
version = "0.1.0"
description = ""
authors = ["ChanceFocus"]

[tool.poetry.dependencies]
python = "^3.7"
docopt = "^0.6.2"
chance-config = "^0.0.11"
{REQUIREMENTS}

[tool.poetry.dev-dependencies]
nose = "^1.3.7"
sphinx = "^2.4.4"
sphinx_rtd_theme = "^0.4.3"
coverage = "^5.0.4"
chance-mock-logger = "^0.0.2"
mock = "^4.0.2"
towncrier = "^19.2.0"
commitizen = "^2.4.2"
black = "^20.8b1"
pre-commit = "^2.7.1"
flake8 = "^3.8.4"

[[tool.poetry.source]]
name = "prod"
url = "https://pypi.cluster.chancefocus.com/chancefocus/prod/+simple/"
default = true

[tool.commitizen]
name = "cz_conventional_commits"
version = "0.1.8"
version_files = [
    "{PROJECT_NAME}/__init__.py",
    "pyproject.toml:version",
    "setup.cfg:version",
    "setup.cfg:release",
    "chart/Chart.yaml:appVersion"
]

[tool.black]
line-length = 79

[tool.poetry.scripts]
{PROJECT_NAME} = '{PROJECT_NAME}.main:main'

[build-system]
requires = ["poetry>=0.12"]
build-backend = "poetry.masonry.api"

[tool.towncrier]
package = "{PROJECT_NAME}"
filename = "CHANGES.rst"
directory = "CHANGES/"
title_format = "{version} ({project_date})"
template = "CHANGES/TEMPLATE.rst"
issue_format = "`#{issue} <{PROJECT_REPO_URL}/issues/{issue}>`_"
