#!/usr/bin/env python
# -*- coding: utf-8 -*-
# File: {FILE_NAME}
# Author: {AUTHOR_NAME}
import httpx
import logging
import mock
import sys

from chanconfig import DynamicConfig
from mock_logger import MockLoggingHandler
from multiprocessing import Process
from nose.tools import assert_equals
from time import sleep

from {PROJECT_NAME} import main

LOGGER = logging.getLogger(__name__)


class TestMain():
    """Functional test class for ``main.main``
    """
    def setUp(self):
        self.handler = MockLoggingHandler(level='DEBUG')
        logger = logging.getLogger(main.__name__)
        logger.addHandler(self.handler)
        self.handler.reset()
        self.config = DynamicConfig([
            'general-api', '{PROJECT_NAME}',
            '{PROJECT_NAME}-api',{EXTRA_DYNAMIC_CONFIGS}
        ],
            "test",
        ).dict
        sys.argv = ['', '--config', 'test']

        LOGGER.info('starting main.main...')
        self.proc = Process(target=main.main, args=(), daemon=True)
        self.proc.start()
        sleep(3)

    def tearDown(self):
        self.proc.terminate()

    def test_main_normal(self):
        """Check if ``main.main`` works
        """
        res = httpx.get('http://127.0.0.1:5000/health')
        data = res.json()
        assert_equals(data, {'status': 'ok'})

    @mock.patch.object(main, 'run')
    def test_main_exception_raised(self, mock_run):
        """Check if `main.main` works when `main.run` raised exceptions

        Args:
            mock_run: the mock obj of `main.run`
        """
        mock_run.side_effect = KeyError('test')

        main.main()

        assert_equals(
            self.handler.messages['info'],
            ['Initialize logger', 'Start', 'End'])

        assert any(
            ["test" in msg for msg in self.handler.messages['error']])
