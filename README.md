# Wheat [![CI](https://git.chancefocus.com/transaction/wheat/badges/master/pipeline.svg)](https://git.chancefocus.com/transaction/wheat/commits/master)[![coverage report](https://git.chancefocus.com/transaction/wheat/badges/master/coverage.svg)](https://git.chancefocus.com/transaction/wheat/commits/master)

Wheat is a Python library for generating python project.

## Prerequisite

- Python ^3.7
- Git
- Poetry
- Python IDE (Visual Studio Code is recommended!)

**⚠️ Read the official manual or documentation of following projects before write any code ⚠️**:

- `git`, a version control system
- `Poetry`, a python library to manage project dependencies
- `towncrier`, a changelog generater
- `pre-commit`, a git commit hook runner
- `commitizen`, a semantic version manager

## Usage

### Install

from pypi:

```bash
pip install chance-wheat
```

from source code:

```bash
poetry install
```

### Run

```bash
wheat
```

## Development

1. Install dev and production dependencies of the project by running:

```bash
poetry install
```

2. Install `pre-commit`

```bash
poetry run pre-commit install
poetry run pre-commit install --hook-type commit-msg
```

3. Verify `pre-commit`

```bash
poetry run pre-commit run --all-files
```

4. Write your code in your IDE (Visual Studio Code is recommended!)
5. Run unittest in your local dev environment

```bash
poetry run nosetests -w tests/unit
```

6. After unittest passed, commit the changes
7. Check if `pre-commit` check passed
8. Push code changes to remote repository
9. If CI passed and code quality is ok, bump version with `commitizen`

```bash
poetry run cz bump --files-only
```

10. Write changelog with `towncrier`

    1.  create a new temporary fragement in `CHANGES/`
    2.  generate changelog

    ```bash
    poetry run towncrier
    ```

    3.  remove temporary fragement

11. Commit and push changes to remote repository

## Contribution Rules

- Clone this repository
- Create `issues` first
- Create `merge request`
- Checkout the auto generated branch in your local environment
- **DO NOT** push directly to `master` branch

## License

[MIT](https://choosealicense.com/licenses/mit/)
